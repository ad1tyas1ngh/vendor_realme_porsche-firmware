# vendor_realme_porsche-firmware

Firmware images for realme GT 2 (porsche), to include in custom ROM builds.

**Current version**: RMX3312_13.1.0.1413(EX01)

### How to use?

1. Clone this repo to `vendor/realme/porsche-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/realme/porsche-firmware/BoardConfigVendor.mk
```
